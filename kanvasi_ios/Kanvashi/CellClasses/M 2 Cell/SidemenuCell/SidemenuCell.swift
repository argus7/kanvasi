//
//  SidemenuCell.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 13/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import UIKit

class SidemenuCell: UITableViewCell {

    @IBOutlet weak var leftIconIV: UIImageView!
    @IBOutlet weak var leftTitlelabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
