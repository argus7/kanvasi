//
//  HomeTableViewCell.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 12/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
       
        self.contentView.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.088)
        
        
//        let shadowLayer = CALayer()
//
//        shadowLayer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1).cgColor
//        shadowLayer.shadowOpacity = 1
//        shadowLayer.shadowOffset = CGSize(width: 0, height: 2)
//        shadowLayer.shadowRadius = 17 / 2
//        shadowLayer.shadowPath = nil
//
//        cardView.layer.insertSublayer(shadowLayer, at: 0)
        cardView.layer.cornerRadius = 15
        cardView.layer.masksToBounds = true
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
