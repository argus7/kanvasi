//
//  PostTypeCell.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 13/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import UIKit

class PostTypeCell: UICollectionViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var postTypelbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.cardView.layer.cornerRadius = 15
        self.cardView.layer.borderColor = UIColor(red: 232.0 / 255.0, green: 180.0 / 255.0 , blue: 198.0 / 255.0, alpha: 1).cgColor
        self.cardView.layer.borderWidth = 0.6
        
        
    }

}
