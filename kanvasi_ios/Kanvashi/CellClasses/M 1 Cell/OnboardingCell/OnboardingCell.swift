//
//  OnboardingCell.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 04/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import UIKit

class OnboardingCell: UICollectionViewCell {

    @IBOutlet weak var onboardingImage: UIImageView!
    @IBOutlet weak var quoteLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = UIColor(named: "appBackGroundColor")
    }

}
