//
//  TermsPrivacyCell.swift
//  Kanvashi
//  r.ranjanchn@gmail.com
//  Created by Veer Chauhan on 10/12/19.
//  Copyright © 2019 Ravi Ranjan. All rights reserved.
//  Nexcode INC
//

import UIKit

class TermsPrivacyCell: UITableViewCell {
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var descriptionlabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
