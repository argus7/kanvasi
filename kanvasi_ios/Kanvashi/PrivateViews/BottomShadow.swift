//
//  BottomShadow.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 17/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import UIKit

class BottomShadow: UIView {
    
    override func draw(_ rect: CGRect) {
        self.backgroundColor = .white
        self.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.2).cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 6/2
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowPath = nil
    }
    
}
