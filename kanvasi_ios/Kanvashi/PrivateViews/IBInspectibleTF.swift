//
//  IBInspectibleTF.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 18/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import UIKit

class IBInspectibleTF: UITextField {
    
    @IBInspectable var borderColor : UIColor? {
        get{
            return UIColor(cgColor: self.layer.borderColor!)
        }
        set{
            self.layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var borderWidth : CGFloat {
        get{
            self.layer.borderWidth
        }
        set{
            self.layer.borderWidth = newValue
        }
    }
}

