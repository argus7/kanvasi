//
//  CircularButton.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 13/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import UIKit

class CircularButton: UIButton {

    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = self.frame.width/2
        
    }

}
