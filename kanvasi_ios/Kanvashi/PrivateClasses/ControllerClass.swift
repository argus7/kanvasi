//
//  ControllerClass.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 05/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import Foundation
import UIKit
class Controller : UIViewController {
    
    
    
    public func setFontPrefrenceAccordingToDevice() -> CGFloat{
        var fontSize = UIFont(name: "Helvetica", size: 14)
        switch UIDevice().type {
        case .iPhone5,.iPhone6, .iPhone7:
            fontSize =  fontSize?.withSize(30)
            return fontSize?.pointSize ?? 14
        case .iPhone6S, .iPhone6plus , .iPhone7plus, .iPhone8plus:
            fontSize = fontSize?.withSize(16)
            return fontSize?.pointSize ?? 16
            
        default:
            return 19
        }
    }
}
