//
//  Viewcontroller + Exten.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 10/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import Foundation
import UIKit
extension UIViewController {
    
    
    /// Navigation bar Prefrence
    /// - Parameter isHidden: Hide for true, visible at False
    public func setNavigationBarPrefrence(_ isHidden : Bool) {
        
        print("\n Debugger : Navigation bar is hidden : \(isHidden)")
        
        self.navigationController?.isNavigationBarHidden =  isHidden
    }
}
