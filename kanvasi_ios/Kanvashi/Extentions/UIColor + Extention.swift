//
//  UIColor + Extention.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 16/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import Foundation
import UIKit
extension UIColor{
    
    open class var notificationViewRightColor: UIColor { get {
        return UIColor(red: 221.0 / 255.0, green: 221.0 / 255.0, blue: 253.0 / 253.0, alpha:1.0)
        }
    }
    
    open class var notificationViewLeftColor: UIColor { get {
        return UIColor(red: 252.0 / 255.0, green: 231.0 / 255.0, blue: 218.0 / 255.0, alpha:1.0)
        }
        
    }
       
    open class var gradientLeftColor: UIColor { get {
        return UIColor(red: 233.0 / 255.0, green: 162.0 / 255.0, blue: 173.0 / 255.0, alpha:1.0)
        }
        
    }
    open class var gradientRightColor: UIColor { get {
        return UIColor(red: 104.0 / 255.0, green: 0.0 / 255.0, blue: 255.0 / 255.0, alpha:1.0)
        }
        
    }
    open class var borderColor : UIColor { get {
        return  UIColor(red: 196.0/255.0, green: 196.0/255.0, blue: 196.0/255.0, alpha: 1)
        }
    }
    open class var gorgotPasswordTextColor : UIColor { get {
        return  UIColor(red: 242.0/255.0, green: 91.0/255.0, blue: 122.0/255.0, alpha: 1)
        }
    }
}
