//
//  String + extention.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 13/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import Foundation
import UIKit

extension NSAttributedString {
    
    open func getAttributedStringWithColor( highlightString: String, normalString: String, highlightColor: UIColor, startingString :  String?) -> NSMutableAttributedString {
           
           let attributedString = NSMutableAttributedString(string: normalString, attributes: [
             .font: UIFont(name: "Montserrat-Medium", size: 14.0)!,
             .foregroundColor: UIColor(red: 44.0 / 255.0, green: 47.0 / 255.0, blue: 59.0 / 255.0, alpha: 1.0),
             .kern: 0.0
           ])
           
        attributedString.addAttribute(.foregroundColor, value: UIColor(white: 149.0 / 255.0, alpha: 1.0), range: NSRange(location: startingString?.count ?? 0, length: highlightString.count))
       
           return attributedString
       }
    
}
