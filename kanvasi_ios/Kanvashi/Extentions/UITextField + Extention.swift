//
//  UITextField + Extention.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 18/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import Foundation
import UIKit
extension UITextField {
    
    
    /**
     This function is used to validate Textfield on the base of their input
     ### Usage ###
     
     - validate the input filled
     
     # Input Type #
     
     - Enum is defined for the input i.e : Validator
     - validator :  email, password, username
     
     # Example #
     ```
     
     let personName = VaildatorFactory.validatorFor(type: validationType.name)
     
     */
    func validatedText(validationType: ValidatorType) throws -> String {
        let validator = VaildatorFactory.validatorFor(type: validationType)
        return try validator.validated(self.text!)
    }
}
