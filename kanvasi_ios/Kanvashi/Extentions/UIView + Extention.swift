//
//  UIView + Extention.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 16/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import Foundation
import UIKit
extension UIView {
    
    func setGradient(cornerRadius : CGFloat, gradientLeftColor : UIColor, gradientRightColor : UIColor) {
        if self.layer.sublayers != nil {
            for gradient in self.layer.sublayers! {
                if ((gradient as? CAGradientLayer) != nil) {
                    gradient.removeFromSuperlayer()
                }
            }
        }
        
        
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [gradientLeftColor.cgColor, gradientRightColor.cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 0.8, y: 1.0)
        gradient.cornerRadius = cornerRadius
        gradient.frame = self.bounds
        
        gradient.opacity = 0.9
        self.layer.insertSublayer(gradient, at: 0)

        self.layoutSubviews()
    }
}
