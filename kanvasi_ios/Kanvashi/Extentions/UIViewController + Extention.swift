//
//  UIViewController + Extention.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 18/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration

extension UIViewController{
    
    /// Get screen width ratio
    open var widthRatio : CGFloat {
        get {
            return UIScreen.main.bounds.width / 320
        }
    }
    
    /// Get current screen Height ratio
    open var heightRatio : CGFloat {
        get {
            return UIScreen.main.bounds.height / 568
        }
    }
    func checkInternet() -> Bool {
        
        if !isConnectedToNetwork() {
            
            return false
        }
        return true
    }
    
    
    func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
    
    
    /// get updated frame of viewController
    ///
    /// - Parameter frame: pass your frame
    /// - Returns: update frame
    func getUpdatedFrame(frame : CGRect) -> CGRect {
        var tempFrame = frame
        tempFrame.size.height *= heightRatio
        tempFrame.size.width *= widthRatio
        return tempFrame
    }
    
    /// get updated width frame of View
    ///
    /// - Parameter frame: pass your frame
    /// - Returns: update  width of the frame
    func getUpdatedWidthFrame(frame : CGRect) -> CGRect {
        var tempFrame = frame
        tempFrame.size.width *= widthRatio
        return tempFrame
    }
    
    
    /// get updated width frame of View
    ///
    /// - Parameter frame: pass your frame
    /// - Returns: update  Height of the frame
    func getUpdatedHeightFrame(frame : CGRect) -> CGRect {
        var tempFrame = frame
        tempFrame.size.height *= heightRatio
        return tempFrame
    }
    
    
    
    
    /// If user login from anothe device
    
//    func showAuthError(){
//        let alerBox = UIAlertController(title: KeyConstants.APP_Name, message: "Looks you have already logged in from another device, you need to login again", preferredStyle: .alert)
//        let logoutAction = UIAlertAction(title: "Ok", style: .destructive) { (action : UIAlertAction) in
//
//            UserDefaults.standard.set(false, forKey: KeyConstants.UserDefaults.kUserRemembered)
//            let vc = self.ElopeStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
//        alerBox.addAction(logoutAction)
//        self.present(alerBox, animated: true)
//    }
}
