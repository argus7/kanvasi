//
//  Enums.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 18/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import Foundation

enum Frame : Int {
    case grid = 0
    case single
}

/// Validation
///
/// - email: textField type Email
/// - password: textField type password
/// - username: textField type username
/// - requiredField: textField type required Field
enum ValidatorType {
    case name
    case email
    case password
    case confirmpassword
    case username
    case otp
    case requiredField(field: String)
}

/// Segment control Indicatior type
///
/// - color: UIColor type
/// - Image: image
enum IndicatorType : Int {
    case color = 1
    case Image
}
/// this Enum property is created for Alert box Type
///
/// - cameraEvent: to upload any image
/// - InfoEvent: to show message

/// - ActionEvent: to perform any action with message
enum  AlertBoxEvent : Double {
    case camera = 0
    case info
    case action
}

/// Compare date with their value
///
/// - lessThan:  Given date is past
/// - greaterThan: Given  Date is in Future
/// - equal:  given date is current date
enum dateComparedBy : Int {
    case lessThan = 1
    case greaterThan
    case equal
    case lessThanEqualTo
    case greaterThanEqualTo
}

/// Get Gender
///
/// - Male: the tag value of male should be 111
/// - Female: the tag value of female should be 11
enum Gender : Int{
    case Male = 111
    case Female = 11
}

enum ResponseCode : Int {
    
    case SUCCESS = 200
    case BAD_REQUEST = 400
    case UNAUTHORIZED = 401
    case FORBIDDEN =  403
    case NOT_FOUND = 404
    case CATCH_BLOCK = 40
    case iOSError = 100
    case FieldError = 202
    case User_Not_Found = 203

}

enum APIError : Error{
    case ResponseFail
    case InvalidJSON
    case Timeout
}
