//
//  KanvashiTFValidator.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 18/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import Foundation

class ValidationError: Error {
    
    var message: String
    
    init(_ message: String) {
        self.message = message
    }
}

protocol ValidatorConvertible {
    func validated(_ value: String) throws -> String
}


enum VaildatorFactory {
    
    /**
     This function is used to return type of Textfield
     ### Usage ###
     
     - Retunr Textfield
     
     # Input Type #
     
     - Enum is defined for the input i.e : Input Type
     - validator :  email, password, username
     
     
     # Failure #
     
     - when input type is not matched it reurn the Error
     
     
     # Example #
     ```
     let personName = validatorFor(type : validatorType.Email
     
     */
    static func validatorFor(type: ValidatorType) -> ValidatorConvertible {
        switch type {
        case .name : return NameValidator()
        case .email: return EmailValidator()
        case .password: return PasswordValidator()
        case .confirmpassword: return ConfirmPasswordValidator()
        case .otp: return OTPValidator()
        case .username: return UserNameValidator()
        case .requiredField(let fieldName): return RequiredFieldValidator(fieldName)
        }
    }
}

struct RequiredFieldValidator: ValidatorConvertible {
    private let fieldName: String
    
    init(_ field: String) {
        fieldName = field
    }
    
    func validated(_ value: String) throws -> String {
        guard !value.isEmpty else {
            throw ValidationError("Required field " + fieldName)
        }
        return value
    }
}


struct NameValidator: ValidatorConvertible {
    
    
    func validated(_ value: String) throws -> String {
        guard value.count >= 3 else {
            throw ValidationError("Name must contain more than three characters." )
        }
        guard value.count < 18 else {
            throw ValidationError("Name shoudn't conain more than 18 characters." )
        }
        
        return value
    }
}

struct UserNameValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        guard value.count >= 3 else {
            throw ValidationError("Username must contain more than three characters." )
        }
        guard value.count < 18 else {
            throw ValidationError("Username shoudn't conain more than 18 characters." )
        }
        do {
            if try NSRegularExpression(pattern: "^[a-z]{1,18}$",  options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                throw ValidationError("Invalid username, username should not contain whitespaces, numbers or special characters.")
            }
        } catch {
            throw ValidationError("Invalid username, username should not contain whitespaces, or special characters.")
        }
        return value
    }
}

struct PasswordValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        guard value != "" else {throw ValidationError("Password is required.")}
        guard value.count >= 8 else { throw ValidationError("Password must have at least 8 characters.") }
        
        do {
            if try NSRegularExpression(pattern: "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$",  options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                throw ValidationError("Password must have at least one letter, one number and one special character.")
            }
        } catch {
            throw ValidationError("Password must have at least one letter, one number and one special character.")
        }
        return value
    }
}

struct ConfirmPasswordValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        guard value != "" else {throw ValidationError("Please confirm your password.")}
        guard value.count >= 8 else { throw ValidationError("Password must have at least 8 characters.") }
        
        do {
            if try NSRegularExpression(pattern: "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$",  options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                throw ValidationError("Password must have at least one letter, one number and one special character.")
            }
        } catch {
            throw ValidationError("Password must have at least one letter, one number and one special character.")
        }
        return value
    }
}

struct OTPValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        guard value != "" else {throw ValidationError("Password is required.")}
        guard value.count >= 4 else { throw ValidationError("OTP must have at least 4 characters.") }
       
        return value
    }
}

struct EmailValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        do {
            if try NSRegularExpression(pattern: "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$", options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                throw ValidationError("Please enter a valid email address")
            }
        } catch {
            throw ValidationError("Please enter a valid email address")
        }
        return value
    }
}

