//
//  KanvashiSideMenuVC.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 13/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import UIKit

class KanvashiSideMenuVC: UIViewController {
    
    @IBOutlet weak var sideMenuTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("\n \u{25ba} \u{25ba}  KanvashiSideMenuVC \u{1f44d}\n\n Debugger : side menu View did load method is called")
        
        // Do any additional setup after loading the view.
        self.setupSideMenuTableView()
    }
    
    fileprivate func setupSideMenuTableView(){
        
        self.sideMenuTable.delegate = self
        self.sideMenuTable.dataSource = self
        self.sideMenuTable.register(UINib(nibName: "SidemenuCell", bundle: Bundle.main), forCellReuseIdentifier: "SidemenuCell")
        
    }
    
    //MARK:-
    //MARK:- Get side bar data
    /// get side bar table data
    fileprivate func getSidebarMenuData() -> (iconImage : [String] , titleName : [String]) {
        
     
        let title = ["Explore","Magnet","Setting","Faq's","Invite a friend","Logout"]
        let icon = ["explore","magnet","setting","faq","inviteAFriend","logout"]
        return(icon , title)
        
    }
    
    
    //MARK:-
    //MARK:- NavigationBar setup
    
    /*
     Navigation bar setup
     disappear : Hidden
     didload : visible
     */
    

    @IBAction func onClickBackButton(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    /// move to Home Tab bar controller
    /// - Parameter sender:Left bar button (back button)
    @objc func movetoParentVC(_ sender : UIBarButtonItem) {
        
        print("\n Debugger : Move to Parent VC is called")
        self.navigationController?.popViewController(animated: true)
    }
}


//MARK:-
//MARK:- Table view data source
/*
 Kanvashi post table data source
 */
extension KanvashiSideMenuVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.getSidebarMenuData().iconImage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let messageCell = tableView.dequeueReusableCell(withIdentifier: "SidemenuCell", for: indexPath) as! SidemenuCell
        messageCell.leftIconIV.image = UIImage(named: getSidebarMenuData().iconImage[indexPath.row])
        messageCell.leftTitlelabel.text = getSidebarMenuData().titleName[indexPath.row]
        if indexPath.row == 5 {
            messageCell.leftTitlelabel.textColor = UIColor.lightGray
        }
        return messageCell
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

//MARK:-
//MARK:- Table view Delegate
/*
 Kanvashi post table Delegate
 */
extension KanvashiSideMenuVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}
