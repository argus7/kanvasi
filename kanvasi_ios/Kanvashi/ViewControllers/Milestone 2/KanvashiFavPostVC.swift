//
//  KanvashiFavPostVC.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 18/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import UIKit

class KanvashiFavPostVC: UIViewController {
    
    @IBOutlet weak var favouriteCV : UICollectionView!
    var postFrame : Frame = .grid

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.favouriteTableSetup()
        self.navigationBarSetup()
        // Do any additional setup after loading the view.
    }
    
    private func navigationBarSetup(){
        self.setNavigationBarPrefrence(false)
        if let navigationBarLayer = self.navigationController?.navigationBar{
            navigationBarLayer.backgroundColor = .white
            navigationBarLayer.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.142).cgColor
            navigationBarLayer.layer.shadowOpacity = 1
            navigationBarLayer.layer.shadowRadius = 5/2
            navigationBarLayer.layer.shadowOffset = CGSize(width: 0, height: 3)
            navigationBarLayer.layer.shadowPath = nil
            //Creating back button to move to previous page
            let backButton = UIBarButtonItem(image: UIImage(named: "leftArrowKey"), style: .plain, target: self, action: #selector(movetoProfileVC(_:)))
            backButton.tintColor = UIColor.lightGray
            self.navigationItem.leftBarButtonItem = backButton
            self.title = "Favourite"

        }
        
    }
    
    /// Navigation Bar left  button setup
    /// - Parameter sender: UIBarButton
    @objc func movetoProfileVC(_ sender : UIBarButtonItem){
        print("\n Debugger : Move to Profile page action called")
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    fileprivate func favouriteTableSetup(){
        self.favouriteCV.delegate = self
        self.favouriteCV.dataSource = self
        self.favouriteCV.register((UINib(nibName: "ProfilePostCell", bundle: Bundle.main)), forCellWithReuseIdentifier: "ProfilePostCell")
    }
    
    //MARK:-
    //MARK:- All IBAction
    
    @IBAction func onClickGrid(_ sender: UIButton) {
        print("\n Debugger : Clicked on Grid Button button")
          if postFrame == .grid {
              print("Already in Grid")
          }else{
              postFrame = .grid
              self.favouriteCV.reloadData()
          }
    }
    @IBAction func onClickSingle(_ sender: UIButton) {
        print("\n Debugger : Clicked on Sibngle Post Button button")
          if postFrame == .single {
              print("Already in Single")
          }else{
              postFrame = .single
              self.favouriteCV.reloadData()
          }
    }
    
}

extension KanvashiFavPostVC : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 50
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let favCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfilePostCell", for: indexPath) as! ProfilePostCell
        //Static images It will change later
        let staticImageArr = ["bitmap","bitmap2","bitmap1","image"]
        let randomImage = staticImageArr.randomElement()!
        print(randomImage)
        favCell.postImage.image = UIImage(named: randomImage)
        
        return favCell
    }
}
extension KanvashiFavPostVC : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch postFrame {
        case .grid:
            let padding: CGFloat =  30
            let collectionViewSize = (collectionView.frame.size.width - padding ) / 2
            return CGSize(width: collectionViewSize, height: collectionViewSize)
        case .single :
            
            let padding: CGFloat =  20
            let collectionViewSize = collectionView.frame.size.width - padding
            return CGSize(width: collectionViewSize, height: collectionViewSize)
        }
    }
}
