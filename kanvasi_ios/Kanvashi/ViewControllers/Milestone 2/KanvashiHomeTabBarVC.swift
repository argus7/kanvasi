//
//  KanvashiHomeTabBarVC.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 12/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import UIKit

class KanvashiHomeTabBarVC: UITabBarController {
    
    @IBOutlet weak var kanvashiTabBar: UITabBar!
    let centerButton = UIButton.init(type: .custom)
    @IBOutlet weak var kanvashiHomeTabBar: UITabBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("\n \u{25ba} \u{25ba}  KanvashiHomeTabBarVC \u{1f44d}\n\n Debugger : HomeTabBar View did load method is called")
        self.settabBarCentreButtonPosition(tintColor: .orange, bartintColor: .white)
        self.setTabBar()
        // Do any additional setup after loading the view.
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBarPrefrence(true)
    }
    
    
    /// Setup Tab bar
    fileprivate func setTabBar(){
        
        print("\n Debugger : Tab Bar setup")
        
        self.tabBar.tintColor = UIColor(red: 255.0 / 255.0, green: 137.0 / 255.0, blue: 147.0 / 255.0, alpha: 1)
        /*
         Home view controller setup
         */
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "KanvashiHomeVC") as! KanvashiHomeVC
        homeVC.tabBarItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "home"), tag: 1)
        homeVC.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: -5, vertical: 0)
        
        /*
         message view controller setup
         */
        
        let messageVC = self.storyboard?.instantiateViewController(withIdentifier: "KanvashiMessageVC") as! KanvashiMessageVC
        messageVC.tabBarItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "chat"), tag: 2)
        messageVC.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: -20, vertical: 0)
        
        /*
         Notification view controller setup
         
         */
        let notificationVC = self.storyboard?.instantiateViewController(withIdentifier: "kanvashiNotificationVC") as! kanvashiNotificationVC
        notificationVC.tabBarItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "notification"), tag: 3)
        notificationVC.tabBarItem.titlePositionAdjustment =  UIOffset(horizontal: 20, vertical: 0)
        
        /*
         Profile view controller setup
         */
        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "kanvashiProfileVC") as! kanvashiProfileVC
        profileVC.tabBarItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "profile"), tag: 4)
        profileVC.tabBarItem.titlePositionAdjustment =  UIOffset(horizontal: 5, vertical: 0)
        
        
        let tabBarControllers = [homeVC, messageVC, notificationVC, profileVC]
        self.viewControllers = tabBarControllers
        
    }
   
    
    /// Setup Tab bar center button
    fileprivate func settabBarCentreButtonPosition(tintColor : UIColor,bartintColor : UIColor){
        
        //Get devive height and Width
        let bounds = UIScreen.main.bounds
        let deviceheight = bounds.size.height
        let deviceWidth = bounds.size.width
        
        print(deviceheight, deviceWidth)
        
        //Craete Y position of center button
        var yPosition = self.view.frame.height - self.kanvashiTabBar.frame.height - 40
        
        if deviceheight > 736{
            yPosition = self.view.frame.height - self.kanvashiTabBar.frame.height - 80
        }
        
        // set frame for center button
        centerButton.frame = CGRect(x: self.tabBar.frame.width / 2 - 40, y: yPosition, width: 80, height: 80)
        centerButton.setBackgroundImage(#imageLiteral(resourceName: "add"), for: .normal)
        centerButton.addTarget(AnyClass.self, action: #selector(onClickCenterTabBarButton(_:)), for: .touchUpInside)
        
        // Add center button to tab bar position center over tab bar
        self.view.insertSubview(centerButton, aboveSubview: self.tabBar)
        // set tint color for tab bar
        self.tabBar.barTintColor = bartintColor
        self.tabBar.tintColor = tintColor
    }
    
    
    @objc func onClickCenterTabBarButton(_ sender : UIButton){
        print("\n Debugger : Tab bar center button is tapped")
    }
    
}
