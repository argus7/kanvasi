//
//  KanvashiMessageVC.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 12/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import UIKit

class KanvashiMessageVC: UIViewController {
    
    @IBOutlet weak var kanvashiMessageTV: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("\n \u{25ba} \u{25ba}  KanvashiMessageVC \u{1f44d}\n\n Debugger : Message View did load method is called")
        
        
        self.setupMessageTableView()
        // Do any additional setup after loading the view.
    }
    
    
    
    fileprivate func getPostTypeArray() -> [String] {
        
        let array  = [" All ", " Post ", " Notice ", " Portfolios "]
        return array
    }
    
    
    fileprivate func setupMessageTableView(){
        
        self.kanvashiMessageTV.delegate = self
        self.kanvashiMessageTV.dataSource = self
        self.kanvashiMessageTV.register(UINib(nibName: "MessageCell", bundle: Bundle.main), forCellReuseIdentifier: "MessageCell")
        
    }
    
    //MARK:-
    //MARK:- All IBActions
    
    @IBAction func onClickMenu(_ sender: UIButton) {
        
        print("\n Debugger : Clicked on Menu button")
        let sideMenuVC = self.storyboard?.instantiateViewController(withIdentifier: "KanvashiSideMenuVC") as! KanvashiSideMenuVC
        self.navigationController?.pushViewController(sideMenuVC, animated: true)
        
    }
    @IBAction func onClickSearch(_ sender: UIButton) {
    }
}


//MARK:-
//MARK:- Table view data source
/*
 Kanvashi post table data source
 */
extension KanvashiMessageVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let messageCell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as! MessageCell
        
        return messageCell
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
}

//MARK:-
//MARK:- Table view Delegate
/*
 Kanvashi post table Delegate
 */
extension KanvashiMessageVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}

