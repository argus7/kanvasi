//
//  kanvashiNotificationVC.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 12/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import UIKit

class kanvashiNotificationVC: UIViewController {
    
    
    var isCellLoaded : Bool = false
    @IBOutlet weak var notificationTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("\n \u{25ba} \u{25ba}  kanvashiNotificationVC \u{1f44d}\n\n Debugger : Notification View did load method is called")
        
        self.setupNotificationTableView()
        // Do any additional setup after loading the view.
        
    }
    
    @IBAction func onClickMenu(_ sender: UIButton) {
        
        print("\n Debugger : Clicked on Menu button")
        let sideMenuVC = self.storyboard?.instantiateViewController(withIdentifier: "KanvashiSideMenuVC") as! KanvashiSideMenuVC
        self.navigationController?.pushViewController(sideMenuVC, animated: true)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        isCellLoaded = true
        self.notificationTable.reloadData()
    }
    
    
    fileprivate func setupNotificationTableView(){
        self.notificationTable.register(UINib(nibName: "NotificationCell", bundle: Bundle.main), forCellReuseIdentifier: "NotificationCell")
        self.notificationTable.delegate = self
        self.notificationTable.dataSource = self
        
    }
}


//MARK:-
//MARK:- Table view data source
/*
 Kanvashi Notification table data source
 */
extension kanvashiNotificationVC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        let notificationCell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        if isCellLoaded {
            notificationCell.gradientView.setGradient(cornerRadius: 5, gradientLeftColor: .notificationViewLeftColor, gradientRightColor: .notificationViewRightColor)
            if let gradient = notificationCell.gradientView.layer.sublayers?[0] as? CAGradientLayer {
                gradient.frame = notificationCell.gradientView.bounds
            }
        }
        
        return notificationCell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
}

//MARK:-
//MARK:- Table view Delegate
/*
 Kanvashi Notification table Delegate
 */
extension kanvashiNotificationVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
}
