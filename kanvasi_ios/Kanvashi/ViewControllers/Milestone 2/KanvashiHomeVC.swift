//
//  KanvashiHomeVC.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 12/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import UIKit

class KanvashiHomeVC: UIViewController {
    @IBOutlet weak var kanvashiPostTypeCV: UICollectionView!
    
    @IBOutlet weak var kanvashiHomeTV: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("\n \u{25ba} \u{25ba}  KanvashiHomeVC \u{1f44d}\n\n Debugger : Home View did load method is called")

        self.setupHomeTableView()
        // Do any additional setup after loading the view.
    }
    
    fileprivate func getPostTypeArray() -> [String] {
        
        let array  = [" All ", " Post ", " Notice ", " Portfolios "]
        return array
    }
    
    
    fileprivate func setupHomeTableView(){
        self.kanvashiHomeTV.delegate = self
        self.kanvashiHomeTV.dataSource = self
        self.kanvashiHomeTV.register(UINib(nibName: "HomeTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "HomeTableViewCell")
        
        
        self.kanvashiPostTypeCV.delegate = self
        self.kanvashiPostTypeCV.dataSource = self
        self.kanvashiPostTypeCV.register(UINib(nibName: "PostTypeCell", bundle: Bundle.main), forCellWithReuseIdentifier: "PostTypeCell")
     
    }
    
    //MARK:-
    //MARK:- All IBactions
    @IBAction func onclickMenu(_ sender: UIButton) {
        
        print("\n Debugger : Clicked on Menu button")
        let sideMenuVC = self.storyboard?.instantiateViewController(withIdentifier: "KanvashiSideMenuVC") as! KanvashiSideMenuVC
        self.navigationController?.pushViewController(sideMenuVC, animated: true)

    }
    
    @IBAction func onClickSearch(_ sender: UIButton) {
        print("\n Debugger : Clicked on Search Button")

    }
    
    
    
}


//MARK:-
//MARK:- Table view data source
/*
 Kanvashi post table data source
 */
extension KanvashiHomeVC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let homeCell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
        
        let attributeString = getAttributedString(highlightString: "shared a", normalString: "Nina dobrev shared a post", highlightColor: .lightGray, startingString: "Nina Dobrev")

      
        homeCell.nameLbl.attributedText = attributeString
        return homeCell
    }
}

//MARK:-
//MARK:- Table view Delegate
/*
 Kanvashi post table Delegate
 */
extension KanvashiHomeVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 420
    }
    
   func getAttributedString( highlightString: String, normalString: String, highlightColor: UIColor, startingString :  String?) -> NSMutableAttributedString {
        
        let attributedString = NSMutableAttributedString(string: normalString, attributes: [
          .font: UIFont(name: "Montserrat-Medium", size: 14.0)!,
          .foregroundColor: UIColor(red: 44.0 / 255.0, green: 47.0 / 255.0, blue: 59.0 / 255.0, alpha: 1.0),
          .kern: 0.0
        ])
        
     attributedString.addAttribute(.foregroundColor, value: UIColor(white: 149.0 / 255.0, alpha: 1.0), range: NSRange(location: startingString?.count ?? 0, length: highlightString.count))
    
        return attributedString
    }
}

//MARK:-
//MARK:- collection view  data source
/*
 Kanvashi post type collection view data source
 */


extension KanvashiHomeVC : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return getPostTypeArray().count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let typeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostTypeCell", for: indexPath) as! PostTypeCell
        
        typeCell.postTypelbl.text = getPostTypeArray()[indexPath.row]
        return typeCell
    }
}
//MARK:-
//MARK:- collection view  Delegate
/*
 Kanvashi post type collection view data source
 */
extension KanvashiHomeVC : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 200, height: 30)
    }
}
