//
//  kanvashiProfileVC.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 12/12/19.
//  Copyright © 2019 argus Veer. All rights reserved.
//

import UIKit

class kanvashiProfileVC: UIViewController {
    
    @IBOutlet weak var postTypeCV: UICollectionView!
    @IBOutlet weak var postCV: UICollectionView!
    var postFrame : Frame = .grid
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("\n \u{25ba} \u{25ba}  kanvashiProfileVC \u{1f44d}\n\n Debugger : Profile View did load method is called")
        
        // Do any additional setup after loading the view.
        
        self.setupCurrentUserProfileCV()
    }
    
    /*
     user profile post and post type collection view setup
     */
    fileprivate func setupCurrentUserProfileCV(){
        
        self.postTypeCV.tag = 10
        self.postCV.tag = 100
        self.postCV.delegate = self
        self.postCV.dataSource = self
        self.postTypeCV.delegate = self
        self.postTypeCV.dataSource = self
        self.postCV.register(UINib(nibName: "ProfilePostCell", bundle: Bundle.main), forCellWithReuseIdentifier: "ProfilePostCell")
        self.postTypeCV.register(UINib(nibName: "PostTypeCell", bundle: Bundle.main), forCellWithReuseIdentifier: "PostTypeCell")
    }
    
    /*
     post type static array setup
     */
    fileprivate func getPostTypeArray() -> [String] {
        
        let array  = [" All ", " Post ", " Notice ", " Portfolios "]
        return array
        
    }
    
    //MARK:-
    //MARK:- All IBActions
    
    @IBAction func onClickSideMenu(_ sender: UIButton) {
        
        print("\n Debugger : Clicked on Menu button")
        let sideMenuVC = self.storyboard?.instantiateViewController(withIdentifier: "KanvashiSideMenuVC") as! KanvashiSideMenuVC
        self.navigationController?.pushViewController(sideMenuVC, animated: true)
        
    }
    @IBAction func onClickEdit(_ sender: UIButton) {
        print("\n Debugger : Clicked on Edit Button button")
        
    }
    
    @IBAction func onClickGrid(_ sender: UIButton) {
        print("\n Debugger : Clicked on Grid Button button")
        if postFrame == .grid {
            print("Already in Grid")
        }else{
            postFrame = .grid
            self.postCV.reloadData()
        }
        
    }
    
    @IBAction func onClickSinglePost(_ sender: UIButton) {
        print("\n Debugger : Clicked on Sibngle Post Button button")
        if postFrame == .single {
            print("Already in Single")
        }else{
            postFrame = .single
            self.postCV.reloadData()
        }
        
    }
    
    @IBAction func onClickFavourite(_ sender: UIButton) {
        print("\n Debugger : Clicked on favourite button")
        let sideMenuVC = self.storyboard?.instantiateViewController(withIdentifier: "KanvashiFavPostVC") as! KanvashiFavPostVC
        self.navigationController?.pushViewController(sideMenuVC, animated: true)
    }
    
}


extension kanvashiProfileVC : UICollectionViewDataSource {
    // TAG 10 -> Posttype CV
    //Tag 100 -> post CV
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        //Diffrenciating collection view with Tag
        
        switch collectionView.tag {
        case 10:
            return self.getPostTypeArray().count
        case 100:
            return 100
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //Diffrenciating collection view with Tag
        switch collectionView.tag {
        case 10:
            let typeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostTypeCell", for: indexPath) as! PostTypeCell
            typeCell.postTypelbl.text = getPostTypeArray()[indexPath.row]
            return typeCell
        case 100:
            let postCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfilePostCell", for: indexPath) as! ProfilePostCell
            let staticImageArr = ["bitmap","bitmap2","bitmap1","image"]
            let randomImage = staticImageArr.randomElement()!
            postCell.postImage.image = UIImage(named: randomImage)
            
            return postCell
        default:
            return UICollectionViewCell()
        }
    }
}

//MARK:-
//MARK:- Collection view Delegate , flow layout delegate setup
extension kanvashiProfileVC :  UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
    
    /*
     tag = 10  for post type cell
     tag = 100 for post cell
     */
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //diffrenciating cell with Tag
        switch collectionView.tag {
        case 10:
            return CGSize(width: 200, height: 30)
        case 100:
            switch postFrame {
            case .grid:
                let padding: CGFloat =  30
                let collectionViewSize = (collectionView.frame.size.width - padding ) / 2
                return CGSize(width: collectionViewSize, height: collectionViewSize)
            case .single :
                
                let padding: CGFloat =  20
                let collectionViewSize = collectionView.frame.size.width - padding
                return CGSize(width: collectionViewSize, height: collectionViewSize)
            }
            
        default:
            return CGSize(width: 0, height: 0)
        }
    }
}
