//
//  SplashVC.swift
//  Kanvashi
//  (r.ranjanchn@gmail.com)
//  Created by Veer Chauhan on 04/12/19.
//  Copyright © 2019 argus Veer . All rights reserved.
//

import UIKit

class SplashVC: UIViewController {
    
    var splashTimer: Timer?
    override func viewDidLoad() {
        super.viewDidLoad()
        print("\n \u{25ba} \u{25ba}  SplashVC \u{1f44d}\n\n Debugger : kanavshi Custom Splash Page is loaded")

        self.splashVCPrefrenceSetup()
    }
    
    /*
     creating timer for delay
     */
    private func splashVCPrefrenceSetup(){
        print("\n Debugger : kanavshi Splash Page  prefrence setup")
        self.navigationController?.isNavigationBarHidden = true
        self.splashTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(spalshTimerAction), userInfo: nil, repeats: false)
    }
    /*
     calling timer action after delay
     */
    @objc func spalshTimerAction(_ sender : Timer){
        print("\n Debugger :splash Time action is called")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OnboardingVC") as! OnboardingVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
}

