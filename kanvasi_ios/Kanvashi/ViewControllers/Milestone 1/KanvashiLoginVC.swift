//
//  KanvashiLoginVC.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 04/12/19.
//  Copyright © 2019 argus Veer.(r.ranjanchn@gmail.com) All rights reserved.
//

import UIKit

class KanvashiLoginVC: UIViewController {
    @IBOutlet weak var emailAddressLbl: UILabel!
    @IBOutlet weak var emailIdTF: UITextField!
    @IBOutlet weak var passwordStaticLbl: UILabel!
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBOutlet weak var signinButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("\n \u{25ba} \u{25ba}  KanvashiLoginVC  \u{1f44d}\n\n Debugger : kanavshi Login Page is loaded")
        // Do any additional setup after loading the view.
        self.kanvashiDarkModePrefrenceSetup()
        
    }
    
    fileprivate func kanvashiDarkModePrefrenceSetup(){
        
        print("\n Debugger : kanavshi Login Page Dark mode prefrence updating")
        
        self.view.backgroundColor = UIColor(named: "appBackGroundColor")
        // self.mainContainerView.backgroundColor = UIColor(named: "appBackGroundColor")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.kanvashiLoginVCPrefrences()
        
    }
    
    
    //MARK: - INSTANCE METHOD
    /*
     TextField Validation
     */
    private func validate() -> Bool {
        do {
            let _ = try emailIdTF.validatedText(validationType: ValidatorType.email)
            let _ = try passwordTF.validatedText(validationType: ValidatorType.password)

            
            return true
        } catch( _) {
            let _ = UIImageView(image: #imageLiteral(resourceName: "danger"))
//            let banner = NotificationBanner(title: "Alert", subtitle: (error as! ValidationError).message, leftView: leftView, style: .warning)
//            banner.show()
            return false
        }
    }
    /*
     setting Navigation Bar prefrence
     */
    private func kanvashiLoginVCPrefrences(){
        
        print("\n Debugger : Kanavashi Login Page prefrence Setting")
        
        self.setNavigationBarPrefrence(true)
    }
    
    
    //MARK:- All IBACTIONS
    
    /*
     Move to sign up page
     */
    @IBAction func onClickSignup(_ sender: UIButton) {
        print("\n Debugger : Sign up button is pressed")
        
        let signupVC = self.storyboard?.instantiateViewController(withIdentifier: "KanvashiRegistrationVC") as! KanvashiRegistrationVC
        self.navigationController?.pushViewController(signupVC, animated: true)
    }
    /*
     Move to forgot Password page
     */
    
    @IBAction func onClickForgotPassword(_ sender: UIButton) {
        print("\n Debugger : Forgot password  button is pressed")
        
        let forgotPassVC = self.storyboard?.instantiateViewController(withIdentifier: "KanvashiForgotPasswordVC") as! KanvashiForgotPasswordVC
        self.navigationController?.pushViewController(forgotPassVC, animated: true)
    }
    /*
     call service for Sign in
     */
    @IBAction func onClickSignIn(_ sender: UIButton) {
        print("\n Debugger : Sign In button is pressed")

        let homeTabBarVC = self.storyboard?.instantiateViewController(withIdentifier: "KanvashiHomeTabBarVC") as! KanvashiHomeTabBarVC
        self.navigationController?.pushViewController(homeTabBarVC, animated: true)
    }
    
}
