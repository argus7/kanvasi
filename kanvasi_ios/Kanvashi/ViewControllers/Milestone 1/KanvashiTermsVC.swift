//
//  KanvashiTermsVC.swift
//  Kanvashi
// (r.ranjanchn@gmail.com)
//  Created by Veer Chauhan on 10/12/19.
//  Copyright © 2019 argus Veer.  All rights reserved.
//

import UIKit

class KanvashiTermsVC: UIViewController {
    
    @IBOutlet weak var termsTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        print("\n \u{25ba} \u{25ba}  KanvashiTermsVC \u{1f44d}\n\n Debugger : Terms View did load method is called")
        
        self.setupKanvashiTermsTable()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        print("\n Debugger : Terms View will appear  method is called")
    
        self.setNavigationBarPrefrence(false)
        self.setNavigationBarPrefrence()
    }

    
    /*
     Terms and condition Table cell registration
     */
    fileprivate func setupKanvashiTermsTable(){
        
        print("\n Debugger : Setting Terms and condition policy table Initilization")
        self.termsTable.dataSource = self
        self.termsTable.delegate = self
        self.termsTable.register(UINib(nibName: "TermsPrivacyCell", bundle: Bundle.main), forCellReuseIdentifier: "TermsPrivacyCell")
    }
    
    /*
     Navigation bar setup
     */
    fileprivate func setNavigationBarPrefrence(){
        
        print("\n Debugger : Setting navigation Bar Prefrence method is called")
        //Creating Back Button on navigation bar
        let backButton = UIBarButtonItem(image: UIImage(named: "leftArrowKey"), style: .plain, target: self, action: #selector(moveToRegistrationVC(_:)))
        self.navigationController?.navigationBar.tintColor = .darkGray
        navigationItem.leftBarButtonItem = backButton
        
    }
    
    /*
     Back button Action Method to pop to Signup page
     */
    @objc func moveToRegistrationVC(_ sender : UIBarButtonItem){
        
        print("\n Debugger : Objective-C selector function is called on pressed back Button")

        self.navigationController?.popViewController(animated: true)
        
    }
}

//MARK:-
//MARK:- Terms and condition table view data source

extension KanvashiTermsVC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let termsCell = tableView.dequeueReusableCell(withIdentifier: "TermsPrivacyCell", for: indexPath) as! TermsPrivacyCell
        
        return termsCell
    }
}
//MARK:-
//MARK:- terms and Condition  table view Delegate


extension KanvashiTermsVC : UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
