//
//  KanvashiRegistrationVC.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 09/12/19.
//  Copyright © 2019 argus Veer. (r.ranjanchn@gmail.com) All rights reserved.
//

import UIKit

class KanvashiRegistrationVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("\n \u{25ba} \u{25ba}  KanvashiRegistrationVC  \u{1f44d}\n\n Debugger : kanavshi Registration Page is loaded")

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBarPrefrence(true)
    }
  
    
    //MARK:- All IBActions
    
    /*
     Move to Sign In Page
     */
    @IBAction func onClickSigninButton(_ sender: UIButton) {
        
        print("\n Debugger : Sign in button is pressed")

        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     Nove to terms and condition Page
     */
    @IBAction func onClickTermsAndCondition(_ sender: UIButton) {
        
        print("\n Debugger : Terms and condition  button is pressed")
        let termsVC = self.storyboard?.instantiateViewController(withIdentifier: "KanvashiTermsVC") as! KanvashiTermsVC
        self.navigationController?.pushViewController(termsVC, animated: true)

    }
    
    /*
     Move to privacy Policy page
     */
    
    @IBAction func onClickPrivacyPolicy(_ sender: UIButton) {
        print("\n Debugger : Privacy Policy  button is pressed")
        let termsVC = self.storyboard?.instantiateViewController(withIdentifier: "KanvashiPrivacyVC") as! KanvashiPrivacyVC
        self.navigationController?.pushViewController(termsVC, animated: true)
    }
    
}
