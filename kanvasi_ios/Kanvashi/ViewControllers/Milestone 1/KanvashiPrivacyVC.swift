//
//  KanvashiPrivacyVC.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 10/12/19.
//  Copyright © 2019 argus Veer. (r.ranjanchn@gmail.com) All rights reserved.
//

import UIKit

class KanvashiPrivacyVC: UIViewController {

    @IBOutlet weak var privacyTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print("\n \u{25ba} \u{25ba}  KanvashiPrivacyVC \u{1f44d}\n\n Debugger : Privacy View did load method is called")
        self.setupKanvashiPrivacyTable()

    }
    /*
     Setting Navigation Bar prefrence
     */
    override func viewWillAppear(_ animated: Bool) {
        print("\n Debugger : Terms View will appear  method is called")
    
        self.setNavigationBarPrefrence(false)
        self.setNavigationBarPrefrence()
    }
    
    /*
     Registrering Terms & Privacy Cell to Table and setting delegate and data source
     */
    fileprivate func setupKanvashiPrivacyTable(){
        print("\n Debugger : Setting Privacxy policy table Initilization")

        self.privacyTable.dataSource = self
        self.privacyTable.delegate = self
        self.privacyTable.register(UINib(nibName: "TermsPrivacyCell", bundle: Bundle.main), forCellReuseIdentifier: "TermsPrivacyCell")
    }
    /*
     Navigation bar setup method
     */
    fileprivate func setNavigationBarPrefrence(){
        
        print("\n Debugger : Setting navigation Bar Prefrence method is called")
        let backButton = UIBarButtonItem(image: UIImage(named: "leftArrowKey"), style: .plain, target: self, action: #selector(moveToRegistrationVC(_:)))
        self.navigationController?.navigationBar.tintColor = .darkGray
        navigationItem.leftBarButtonItem = backButton
        
    }
    /*
     Objective-C Function for Back button
     */
    @objc func moveToRegistrationVC(_ sender : UIBarButtonItem){
        
        print("\n Debugger : Objective-C selector function is called on pressed back Button")
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK:-
//MARK:- Privacy policy  table view data source

extension KanvashiPrivacyVC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let termsCell = tableView.dequeueReusableCell(withIdentifier: "TermsPrivacyCell", for: indexPath) as! TermsPrivacyCell
        
        return termsCell
    }
}

//MARK:-
//MARK:- Privacy policy  table view Delegate


extension KanvashiPrivacyVC : UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
