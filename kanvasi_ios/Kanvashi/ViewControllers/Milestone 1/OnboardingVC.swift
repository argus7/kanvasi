//
//  OnboardingVC.swift
//  Kanvashi
//  (r.ranjanchn@gmail.com)
//  Created by Veer Chauhan on 04/12/19.
//  Copyright © 2019 argus Veer.  All rights reserved.
//

import UIKit

class OnboardingVC: UIViewController {
    
    @IBOutlet weak var kanvashiOnboardingCV: UICollectionView!
    @IBOutlet weak var kanvashiPageControl: UIPageControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("\n \u{25ba} \u{25ba}  OnboardingVC \u{1f44d}\n\n Debugger : Onboarding View did load method is called")
        
        self.OnBoardingVCDarkModeSetup()
        self.prepareOnboardingColletionView()
        self.kanvashiPageControl.numberOfPages = self.getOnboardingData().onBoardingimage.count
        self.navigationController?.isNavigationBarHidden = false
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.onboardingNavigationBarSetup()
    }
    
    /*
     Dark mode view setup
     */
    fileprivate func OnBoardingVCDarkModeSetup(){
        self.view.backgroundColor = UIColor(named: "appBackGroundColor")
        self.kanvashiOnboardingCV.backgroundColor = UIColor(named: "appBackGroundColor")
    }
    
    
    /*
     setting navigation bar
     */
    private func onboardingNavigationBarSetup(){
        
        print("\n Debugger : Onboarding Navigation Bar setup metho called")
        
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.backgroundColor = .white
        let backButton = UIBarButtonItem(title: "", style: .plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        let rightButton = UIBarButtonItem(title: "Skip", style: .plain, target: self, action: #selector(movetoLoginVC(_:)))
        rightButton.tintColor = .lightGray
        navigationItem.rightBarButtonItem = rightButton
        
    }
    
    /// Navigation Bar Right skip button setup
    /// - Parameter sender: UIBarButton
    @objc func movetoLoginVC(_ sender : UIBarButtonItem){
        
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "KanvashiLoginVC") as! KanvashiLoginVC
        self.navigationController?.pushViewController(loginVC, animated: true)
        print("\n Debugger : Move to login action called")
    }
    
    /*
     creating data for onboarding Image which is static
     */
    private func getOnboardingData() -> (onBoardingimage : [String], Quote:  [String]) {
        //Array of Onboarding Image
        let imageArray = ["createShow","spreadSearch","searchFav"]
        
        //Quote for onboarding Image
        let quoteMessageArray = ["Create and Show! Create portfolio and show via Spotlight. Create notices and show via magnet.","Spread your reach through recommendation, followers and socialize yourself and others.","Search for your favorite people nearby you or anywhere as per your requirements." ]
        
        return(imageArray, quoteMessageArray)
        
    }
    
    
    
    /// Preapare Collection View For Onboarding View
    
    fileprivate func prepareOnboardingColletionView(){
        print("\n Debugger : Preparing on Boarding Collection View")
        
        self.kanvashiOnboardingCV.delegate = self
        self.kanvashiOnboardingCV.dataSource = self
        self.kanvashiOnboardingCV.register(UINib(nibName: "OnboardingCell", bundle: Bundle.main), forCellWithReuseIdentifier: "OnboardingCell")
        self.kanvashiOnboardingCV.isPrefetchingEnabled = false
    }
}

//MARK:-
//MARK:- Kanvashi Collection View Datasource

extension OnboardingVC : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.getOnboardingData().onBoardingimage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        print("\n Debugger : Preparing Cell for index \(indexPath.row)")
        
        let onboardingCell = collectionView.dequeueReusableCell(withReuseIdentifier: "OnboardingCell", for: indexPath) as! OnboardingCell
        onboardingCell.quoteLabel.text = getOnboardingData().Quote[indexPath.row]
        onboardingCell.onboardingImage.image = UIImage(named: getOnboardingData().onBoardingimage[indexPath.row])

        return onboardingCell
    }
    
    /// Page controller setup
    /// - Parameter scrollView: Scroll view content off set position 
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        let horizontalCenter = width / 2
        kanvashiPageControl.currentPage = Int(offSet + horizontalCenter) / Int(width)
    }
    
}

//MARK:-
//MARK:- Kanvashi Collection View Flow layout Delegate
extension OnboardingVC : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: kanvashiOnboardingCV.frame.size.width , height: kanvashiOnboardingCV.frame.size.height)
    }
}
