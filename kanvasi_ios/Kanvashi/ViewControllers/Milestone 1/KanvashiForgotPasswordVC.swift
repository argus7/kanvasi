//
//  KanvashiForgotPasswordVC.swift
//  Kanvashi
//
//  Created by Veer Chauhan on 09/12/19.
//  Copyright © 2019 argus Veer.  (r.ranjanchn@gmail.com) All rights reserved.
//

import UIKit

class KanvashiForgotPasswordVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("\n \u{25ba} \u{25ba}  KanvashiForgotPasswordVC \u{1f44d}\n\n Debugger : Forgot password ViewdidLoad Called")
        // Do any additional setup after loading the view.
        self.ForgotPasswordNavigationBarSetup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBarPrefrence(false)
    }
    
    /*
     Forgot Password navigation bar setup
     */
    private func ForgotPasswordNavigationBarSetup(){
        print("\n Debugger : Onboarding Navigation Bar setup method called")
        //Creating back button to move to previous page
        let backButton = UIBarButtonItem(image: UIImage(named: "leftArrowKey"), style: .plain, target: self, action: #selector(movetoLoginVC(_:)))
        backButton.tintColor = UIColor.lightGray
        self.navigationItem.leftBarButtonItem = backButton
        
    }
    
    //MARK:-
    //MARK:- All IBActions
    
    /*
     Move to sign in button
     */
    @IBAction func onClickSigninButton(_ sender: UIButton) {
        print("\n Debugger : Sign in Button is pressed")
        self.navigationController?.popViewController(animated: true)
    }
    

    
    /// Navigation Bar Right skip button setup
    /// - Parameter sender: UIBarButton
    @objc func movetoLoginVC(_ sender : UIBarButtonItem){
        print("\n Debugger : Move to login page action called")

        self.navigationController?.popViewController(animated: true)
    }
    
    
    
}
